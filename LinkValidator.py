from bs4 import BeautifulSoup
from urllib.parse import urlparse,urljoin
class LinkValidator:
    def __init__(self,html,url):
        self.html = html
        self.domain = urlparse(url).netloc
        self.scheme = urlparse(url).scheme
    def getvalidlinks(self):    
        soup = BeautifulSoup(self.html,'lxml')
        links = soup.find_all('a',href=True)
        links_to_return = []
        for link in links:
            link = link['href']
            if link.startswith("/") or link.startswith("#"):
                link = urljoin(self.scheme+"://"+self.domain,link)
            if not link.__contains__("?") and urlparse(link).netloc == self.domain:
                links_to_return.append(link)
        return links_to_return