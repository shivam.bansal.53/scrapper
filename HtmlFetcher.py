import requests
import urllib.request
import random
from DatabaseHandeler import DatabaseHandeler
from configparser import ConfigParser
class HtmlFetcher:
    def __init__(self,url,domain):
        self.url = url
        self.header = {
            'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
            'Accept-Language' : 'en-gb',
            'Accept' : 'test/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',	
        }
        self.domain = domain
        config = ConfigParser()
        config.read('./config.ini')
        username = str(config["LUMINATI CONFIG"]["USERNAME"])
        password = str(config["LUMINATI CONFIG"]["PASSWORD"])
        password = 'yf2tgixl8dgn'
        self.proxy_handler = {
            'http': ('http://%s-session-%s:%s@zproxy.luminati.io:22225' %
                        (username, random.random(),password)),
            'https': ('https://%s-session-%s:%s@zproxy.luminati.io:22225' %
                        (username, random.random(),password)),
        }

    def gethtml(self): 
        try:
            html = requests.get(self.url,headers=self.header,proxies = self.proxy_handler)
            print("scrapping"+str(self.url))
            return html.text
        except Exception as e:
            print("unable to fetch" + str(self.url))
            print(e)
            file = open('logs.txt','a')
            file.write(self.url+"=>>"+str(e)+'\n')
            file.close()
            return None


    def fetch(self):
        html = self.gethtml()
        if html:
            return html
        else:
            return None



