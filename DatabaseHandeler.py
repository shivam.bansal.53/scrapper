import pymongo
class DatabaseHandeler:
    def __init__(self,domain):
        self.mongoclient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.db = self.mongoclient["ScrappingData"]
        self.collection = self.db[domain]
        self.collection.create_index([("link",pymongo.DESCENDING)],unique=True)
    def insert(self,link,html):
        try:
            self.collection.insert({
                "link":link,
                "content":html
            })
        except Exception as e:
            print(e)
            pass    
    def update(self,link,html,change=False):
        try:
            if change:
                self.collection.update({"link":link},
                {
                    "$set":{
                        "content":html,
                        "ischanged":True
                    }
                })
            else:
                self.collection.update({"link":link},
                {
                    "$unset":{
                        "ischanged":1
                    }
                })
        except Exception as e:
            print(e)
            pass

    def transction(self,link,html):
        status = self.verifyifchanged(link,html)
        if status == "update":
            self.update(link,html,True)
        elif status == "insert":
            self.insert(link,html)
        elif status == "no change":
            self.update(link,html)

    def verifyifchanged(self,link,html):
        try:
            db_html = self.collection.find_one({"link":link},{"_id":0})
            if db_html:
                if self.perc_change(db_html['content'],html) < 10 :
                    return "no change"
                else:
                    return "update"
            else:
                return "insert"
        except Exception as e:
            print(e)
            return None
    
    def perc_change(self,html1,html2):
        len1 = len(html1)
        len2 = len(html2)
        diff =0
        if(len1 != len2):
            change = (abs(len1-len2)/(len1 if len1>len2 else len2))*100
        else : 
            for i in range(0,len1 if len1>len2 else len2):
                if(html1[i] != html2[i]):
                    diff = diff+1
            change = (diff/len(html1))*100
        return change

