from Crawler import Crawler
from queue import Empty
from threading import Thread
from multiprocessing import Process,Queue,Manager
import queue
from urllib.parse import urlparse
import os
from configparser import ConfigParser
class EmptyQueueException(Exception):
    pass
class RunCrawlers:

    def __init__(self):
        self.to_scrape_queue = Manager().list()
        self.scraped_set = Manager().list()
        # self.max_threads=2
        cpu_count = os.cpu_count()
        config = ConfigParser()
        config.read('./config.ini')
        if cpu_count:
            self.max_processes= os.cpu_count()
        else:
            self.max_processes= int(config["APP CONFIG"]["CPU_COUNT"])      
        self.max_threads = int(config["APP CONFIG"]["MAX_THREADS_PER_CPU"])


    def initiatescrapequeue(self):
        file = open("seed_urls.txt")
        urls = file.read().split("\n")  
        if(len(urls)>0):
            for url in urls:
                if (len(url)>0):
                    self.to_scrape_queue.append(url)
            try:
                if(len(self.to_scrape_queue)==0):
                    raise EmptyQueueException
                url = self.to_scrape_queue.pop(0)
                links = Crawler(url,urlparse(url).netloc).run()
                if links != None:
                    for link in links:
                        if link not in self.scraped_set and link not in self.to_scrape_queue:
                            self.to_scrape_queue.append(link)
            except EmptyQueueException:
                print("Empty Queue")
                os._exit(0)


    def initworkers(self):
        workerlist = []
        while True:
            try:
                while len(workerlist) < self.max_threads:
                    for _ in range(self.max_threads-len(workerlist)):
                        p = Thread(target=self.worker)
                        p.daemon = True
                        workerlist.append(p)
                        p.start()
                for t in workerlist:
                    t.join()
            except Exception as e:
                print(e)
                return  
    
    def initprocesses(self):
        self.initiatescrapequeue()
        processlist =[]
        for _ in range(self.max_processes):
            p = Process(target=self.initworkers)
            processlist.append(p)
            p.start()
        for p in processlist:
            p.join()

    def worker(self):
        while True:
            try:
                if(len(self.to_scrape_queue)==0):
                    raise EmptyQueueException
                url = self.to_scrape_queue.pop(0)
                if url not in self.scraped_set:
                    self.scraped_set.append(url)
                    print("pages scraped : "+str(len(self.scraped_set)))
                    links = Crawler(url,urlparse(url).netloc).run()
                    if links != None:
                        for link in links:
                            if link not in self.scraped_set and link not in self.to_scrape_queue:
                                self.to_scrape_queue.append(link)
            except EmptyQueueException:
                print("Empty Queue")
                os._exit(0)  
            except Exception as e:
                print(e)
                
if __name__ == "__main__":    
    runner = RunCrawlers()
    runner.initprocesses()
