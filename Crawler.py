import threading
from HtmlFetcher import HtmlFetcher
from LinkValidator import LinkValidator
from DatabaseHandeler import DatabaseHandeler
from queue import Queue

class Crawler():
    def __init__(self,url,domain):
        self.url = url
        self.domain = domain
    def run(self):  
        html = HtmlFetcher(self.url,self.domain).fetch()
        if html != None :
            links = LinkValidator(html,self.url).getvalidlinks()
            DatabaseHandeler(self.domain).transction(self.url,html)
            return links
        else:
            return None
       
